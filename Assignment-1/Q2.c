//program to find area of a disk
#include<stdio.h>

int main()
{
	float radius,area;
	const double PI=3.14;
	
	printf("Enter radius of the disk");
	scanf("%f",&radius);
	
	area=PI*radius*radius;
	
	printf("Area of the disk is %f",area);
	
	return 0;
	
}
