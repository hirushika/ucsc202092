
// Write a C program that takes a number from the user and checks whether that number is either positive or negative or zero

#include<stdio.h>

int main ()
{
	int number;
	printf("Enter a number");
	scanf("%d",&number);
	
	if(number>0)
	{
		printf(" %d is a positive number",number);
	}
	else if(number<0)
	{
		printf("%d is a negative number",number);
	}
	else
	{
		printf("%d is zero",number);
	}
	
	return 0;
	
}
