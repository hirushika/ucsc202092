// Write a C program to check a given character is Vowel or Consonant

#include <stdio.h>
int main()
{
	char letter;
	
	printf("Enter a letter : ");
	scanf("%c", &letter);

     
    if((letter >='a' && letter <= 'z') || (letter >='A' && letter <= 'Z'))
    {
    	
    	switch(letter)
    	{
    		case 'a':
    		case 'e':
			case 'i':
			case 'o':
			case 'u':
			case 'A':
			case 'E':
			case 'I':
			case 'O':
			case 'U':
				printf("Letter is a vowel");
				break;
			default :
				printf("Letter is a consonant");
				
		}
    }
	else
		printf("Not a letter");
    		
	return 0;
}

