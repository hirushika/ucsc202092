//By given an integer value n, program to print multiplication tables from 1 to n

#include<stdio.h>

int main()
{
	int number,x,y,multiplication;
	
	printf("Enter integer value \n");
	scanf("%d",&number);
	
	for(x=1;x<=number;x++)
	{
		printf("Multiplication table of %d \n",x);
		for(y=1;y<=12;y++)
		{
			multiplication=x*y;
			printf("%d * %d = %d\n",x,y,multiplication);
		}
		printf("\n");
	}
	
	return 0;
}
