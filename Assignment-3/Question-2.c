//program to determine the given number is a prime number

#include <stdio.h>
int main()
{
	
	int number, i, count= 0;
    printf("Enter a number ");
    scanf("%d", &number);

    for(i=1;i<=number;i++)
    {
    	if(number%i==0)
    	{
    		count++;
		}
	}
	
	if(count==2)
		printf("%d is a prime number",number);
	else
		printf("%d is not a prime number",number);
		
		
    return 0;
}